<p>Based on the <a href="https://owasp.org/www-project-top-ten/" target="_blank" rel="noopener">OWASP Top Ten</a>, 94% of tested applications exhibit some form of injection vulnerability, with SQL injections being particularly prevalent. Many of us have encountered, and perhaps even written, code that is vulnerable to SQL injection, as avoiding such vulnerabilities can be challenging.</p>
<p>Frameworks like Django, which incorporate Object-Relational Mappers (ORMs), put significant emphasis on preventing SQL injections. However, there are situations where writing raw SQL queries is necessary. In these instances, it is crucial to understand how to avoid SQL injection vulnerabilities. Let's see an example.</p>
<p>
    <code>
        from django.db import models


        class Person(models.Model):
            username = models.CharField(...)
            role = models.CharField (...)

            def print_person_by_role(role: str):
                ''' Print a formatted username-role pairs"""
                for p in Person.objects.raw(f"SELECT * FROM myapp_person WHERE role = '{role}'"):
                print(f"{p.username} has the {p.role} role")
    </code>
</p>
<p>What's wrong with the code above? We passed the role parameter to the SQL statement directly in <strong>line 11</strong>. If this is done in an API, that can cause huge troubles. In some cases, it could result in <a href="https://www.cloudflare.com/learning/security/what-is-remote-code-execution/#:~:text=A%20remote%20code%20execution%20(RCE)%20attack%20is%20one%20where%20an,malware%20or%20stealing%20sensitive%20data." target="_blank" rel="noopener">RCE (Remote Code Execution)</a> attacks. If we call the function as below, we could drop the table instead of reading, or we could dump the “users” table with all the hashed passwords, and could start cracking them using tools, like Hash Cat or John the ripper.</p>
<blockquote>
<p><code>print_person_by_role("' OR 1; DROP TABLE myapp_person; --")</code></p>
</blockquote>
<p>The SQL statement in this case is vulnerable for SQL injections, even though it is technically executed by the Django ORM. To prevent injection, we can use the built-in argument-passing feature of the “raw” function, as you can see below.</p>
<p>
    <code>
        from django.db import models

        class Person (models Model) :
            username = models.CharField(...)
            role = models.CharField(...)
        
            def print_person_by_role(role: str):
                ''" Print a formatted username-role pairs"""
                for p in Person objects.raw("SELECT * FROM myapp_person WHERE role = %s", [role]):
                print(f"{p.username} has the {p.role} role")
    </code>
</p>
<p>As you see, the code is a bit simpler and even not vulnerable anymore. It is rare that we have to use raw SQL statements, especially that they mostly hard-code the table and column names; however, in some cases when performance matters or for complex queries, we may have no other choice. In any case, during a review, make sure to take extra care about them and always look up the <a href="https://docs.djangoproject.com/en/5.0/topics/db/sql/" target="_blank" rel="noopener">related documentation</a>.</p>